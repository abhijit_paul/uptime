FROM node:argon

WORKDIR /app

RUN git clone git://github.com/fzaninotto/uptime.git /app

RUN apt-get update

RUN apt-get -qq install mongodb

RUN npm install

# ENV MONGODB_HOST_PORT localhost:27017
# ENV MONGODB_DATABASE uptime

ENV NODE_ENV=production

# ENV DEBUG Y

VOLUME /data/db/

ENTRYPOINT wget -O /app/config/production.yaml "http://configurator.devops-config/api/v1/config/uptime?spec=^1.0.0&env=$ENV" \
	&& rm /app/config/default.yaml /app/config/test.yaml \
	&& mongod --port 27017 --fork --syslog \
	&& npm start

EXPOSE 8082